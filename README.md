## Features

- Symfony 6
- Twig

## Instalation

- composer install

## About Project

Type a word and get score for it. Rules for points
- 1 point for each unique letter.
- 2 extra points if the word is “almost palindrome”. Definition of “almost palindrome”: if by removing
  at most one letter from the word, the word will be a true palindrome.
- Give 3 extra points if the word is a palindrome.