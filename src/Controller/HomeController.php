<?php

namespace App\Controller;

use App\Service\IsWordEnglishService;
use App\Service\ScoreService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    public function __construct(public IsWordEnglishService $checkEnglishService, public ScoreService $scoreService)
    {}

    #[Route('/', name: 'home')]
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }

    #[Route('/api/score/{word}', name: 'score_word')]
    public function scoreWord(string $word): JsonResponse
    {
        $isEnglish = $this->checkEnglishService->isWordInEnglish($word);

        if (!$isEnglish)
        {
            return $this->json([
                'message' => sprintf('%s is not in English dictionary', $word),
            ], Response::HTTP_BAD_REQUEST);
        }

        $score = $this->scoreService->scoreWord($word);

        return $this->json([
            'score' => $score
        ]);
    }

}