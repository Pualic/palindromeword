<?php

namespace App\Service;

class CheckPalindromeService
{
    public function scorePalindromeWord(string $word): bool
    {
        $reverse = strrev($word);
        return $word === $reverse;
    }

    public function scoreAlmostPalindromeWord(string $word): bool
    {
        $length = strlen($word);
        $isAlmostPalindrome = false;

        for ($char = 0; $char < $length; $char++)
        {
            $removedLetter = substr_replace($word, '', $char, 1);
            $reverseRemovedLetter = strrev($removedLetter);

            if ($removedLetter == $reverseRemovedLetter)
            {
                $isAlmostPalindrome = true;
                break;
            }
        }
        return $isAlmostPalindrome;
    }

}