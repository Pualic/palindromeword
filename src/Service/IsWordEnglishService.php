<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class IsWordEnglishService
{
    public function __construct(private readonly HttpClientInterface $client)
    {}

    public function isWordInEnglish(string $word): bool
    {
        $response = $this->client->request(
            'GET',
            sprintf('https://api.dictionaryapi.dev/api/v2/entries/en/%s', $word)
        );

        return $response->getStatusCode() === 200;
    }

}