<?php

namespace App\Service;

class ScoreService
{
    public const POINTS_FOR_PALINDROME = 3;
    public const POINTS_FOR_ALMOST_PALINDROME = 2;
    public const NO_POINTS = 0;

    public function __construct(public CheckPalindromeService $palindromeService, public ScoreUniqueLettersService $uniqueWordService)
    {}

    public function scoreWord(string $word): int
    {

        $totalScore = ($this->palindromeService->scorePalindromeWord($word)) ? self::POINTS_FOR_PALINDROME : self::NO_POINTS;

        if (!$totalScore)
        {
            $totalScore += ($this->palindromeService->scoreAlmostPalindromeWord($word)) ? self::POINTS_FOR_ALMOST_PALINDROME : self::NO_POINTS;
        }

        $totalScore += $this->uniqueWordService->scoreUniqueLetters($word);

        return $totalScore;
    }

}