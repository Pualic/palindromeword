<?php

namespace App\Service;

class ScoreUniqueLettersService
{
    public function scoreUniqueLetters(string $word): int
    {
        $word = strtolower($word);

        if (str_contains($word, '-'))
        {
            $word = str_replace('-', '', $word);
        }

        $length = strlen($word);
        $uniqueCounter = 0;

        for ($i = 0; $i < $length; $i++)
        {
            if (substr_count($word, $word[$i]) < 2)
            {
                $uniqueCounter++;
            }
        }

        return $uniqueCounter;
    }
}