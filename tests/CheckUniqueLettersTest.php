<?php

namespace App\Tests;

use App\Service\ScoreUniqueLettersService;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CheckUniqueLettersTest extends KernelTestCase
{
    public ContainerInterface $container;

    public function setUp(): void
    {
        parent::setUp();
        $this->container = static::getContainer();
    }

    public function testWordWithUniquerLetters()
    {
        $service = $this->container->get(ScoreUniqueLettersService::class);

        self::assertSame(1, $service->scoreUniqueLetters('level'));
    }

    public function testWordWithNoUniquerLetters()
    {
        $service = $this->container->get(ScoreUniqueLettersService::class);

        self::assertSame(0, $service->scoreUniqueLetters('leel'));
    }

    public function testWordWithUppercaseLetter()
    {
        $service = $this->container->get(ScoreUniqueLettersService::class);

        self::assertSame(1, $service->scoreUniqueLetters('Level'));
    }

    public function testWordWithUppercaseLetters()
    {
        $service = $this->container->get(ScoreUniqueLettersService::class);

        self::assertSame(1, $service->scoreUniqueLetters('LEVEL'));
    }

    public function testWordWithThreeSameLetter()
    {
        $service = $this->container->get(ScoreUniqueLettersService::class);

        self::assertSame(3, $service->scoreUniqueLetters('bookkeeper'));
    }

}