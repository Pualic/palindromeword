<?php

namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerTest extends WebTestCase
{
    public function testHome(): void
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
    }

    public function testEnglishWord(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/level');

        $this->assertResponseIsSuccessful();
    }

    public function testNonEnglishWord(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/radi');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testEnglishWordAndNumber(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/twenty1');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testNumber(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/12345');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testEmptyInput(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/ ');

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testHyphenatedWord(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/score/twenty-one');

        $this->assertResponseIsSuccessful();
    }

}