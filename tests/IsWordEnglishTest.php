<?php

namespace App\Tests;

use App\Service\IsWordEnglishService;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IsWordEnglishTest extends KernelTestCase
{
    public ContainerInterface $container;

    public function setUp(): void
    {
        parent::setUp();
        $this->container = static::getContainer();
    }

    public function testRegularWord(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertTrue($service->isWordInEnglish('level'));
    }

    public function testMissSpelledWord(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('levl'));
    }

    public function testHyphenatedWord(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertTrue($service->isWordInEnglish('twenty-one'));
    }

    public function testTwoWordsTogether(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('twentyone'));
    }

    public function testTwoWordsSeparated(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('twenty one'));
    }

    public function testNonEnglishWord(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('raditi'));
    }

    public function testNumbers(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish(123456));
    }

    public function testNumbersAsString(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('123456'));
    }

    public function testWordAndNumber(): void
    {
        $service = $this->container->get(IsWordEnglishService::class);

        self::assertFalse($service->isWordInEnglish('twenty 1'));
    }
}