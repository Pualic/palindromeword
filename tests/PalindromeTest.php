<?php

namespace App\Tests;

use App\Service\CheckPalindromeService;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

class PalindromeTest extends KernelTestCase
{
    public ContainerInterface $container;

    public function setUp(): void
    {
        parent::setUp();
        $this->container = static::getContainer();
    }

    public function testPalindromeWord(): void
    {
        $service = $this->container->get(CheckPalindromeService::class);

        self::assertTrue($service->scorePalindromeWord('level'));
    }

    public function testNonPalindromeWord(): void
    {
        $service = $this->container->get(CheckPalindromeService::class);

        self::assertFalse($service->scorePalindromeWord('work'));
    }

    public function testAlmostPalindromeWord(): void
    {
        $service = $this->container->get(CheckPalindromeService::class);

        assertTrue($service->scoreAlmostPalindromeWord('levell'));
    }

    public function testNonAlmostPalindromeWord(): void
    {
        $service = $this->container->get(CheckPalindromeService::class);

        assertFalse($service->scoreAlmostPalindromeWord('blevela'));
    }
}